package com.order.service;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.doNothing;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;

import com.order.domain.dto.response.OrderResponseDto;
import com.order.domain.model.Order;
import com.order.exception.BusinessException;
import com.order.feature.ScenarioFactory;
import com.order.repository.OrderRepository;
import com.order.validator.OrderValidator;

import lombok.var;

@RunWith(MockitoJUnitRunner.class)
public class OrderServiceTest {
	@InjectMocks
	private OrderService orderService;

	@Mock
	private OrderRepository orderRepository;

	@Mock
	private ModelMapper modelMapper;

	@Mock
	private OrderValidator orderValidator;

	@Test
	public void getByOrder_WhenSendIdOrderValid_ExpectedSucess() {
		var order = ScenarioFactory.newOrder();
		var optionalOrder = ScenarioFactory.newOptionalOrder();
		var orderResponseDto = ScenarioFactory.orderResponseDto();
		when(orderRepository.findById(order.getId())).thenReturn(optionalOrder);
		when(modelMapper.map(order, OrderResponseDto.class)).thenReturn(orderResponseDto);
		orderService.getByOrder("01");
		verify(orderRepository, times(1)).findById("01");
		verify(modelMapper, times(1)).map(order, OrderResponseDto.class);

	}

	@Test
	public void getByOrder_WhenSendIdOrderInvalid_ExpectedException() {
		var optionalOrderNullo = ScenarioFactory.optionalOrderNullo();
		when(orderRepository.findById("03")).thenReturn(optionalOrderNullo);
		assertThatThrownBy(() -> orderService.getByOrder("03")).isInstanceOf(BusinessException.class)
				.hasMessage("Order não encontrado");

	}

	@Test
	public void save_WhenReceivedOrderRequestDtoValidAndIdProductAndIdOferIsValid_ExpectedSucess() {
		var order = ScenarioFactory.newOrder();
		var orderRequestDto = ScenarioFactory.orderRequestDto();
		var orderResponseDto = ScenarioFactory.orderResponseDto();
		when(modelMapper.map(orderRequestDto, Order.class)).thenReturn(order);
		doNothing().when(orderValidator).validatorProduct(orderRequestDto);
		doNothing().when(orderValidator).validatorOffer(orderRequestDto);
		when(modelMapper.map(order, OrderResponseDto.class)).thenReturn(orderResponseDto);
		orderService.save(orderRequestDto);
		verify(modelMapper, times(1)).map(orderRequestDto, Order.class);
	}

	@Test
	public void save_WhenReceivedOrderRequestDtoInValidOrIdProductAndIdOferIsInValid_ExpectedException() {
		var order = ScenarioFactory.newOrder();
		var orderRequestDto = ScenarioFactory.orderRequestDto();
		var orderResponseDto = ScenarioFactory.orderResponseDto();
		// doNothing().when(orderValidator).validator(orderRequestDto);
		when(modelMapper.map(orderRequestDto, Order.class)).thenReturn(order);
		// when(orderRepository.save(order)).thenThrow(new Th)
		// when(modelMapper.map(order,
		// OrderResponseDto.class)).thenReturn(orderResponseDto);
		// assertThatThrownBy(() ->
		// orderService.save(order)).isInstanceOf(Throwable.class);
	}

	@Test
	public void findByDescription_WhenCallMethod_ExpectedSucess() {
		var newPageable = ScenarioFactory.newPageable();
		var newPage = ScenarioFactory.newPage();
		var description = "Tomada para pele";
		when(orderRepository.findByDescriptionContainingIgnoreCase(description, newPageable)).thenReturn(newPage);
		orderService.findByDescription(description, newPageable);
		verify(orderRepository, times(1)).findByDescriptionContainingIgnoreCase(description, newPageable);
	}

}
