package com.order.domain.dto.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class OrderRequestUpdateDto {
	@ApiModelProperty(value = "id do produto", example = "123")
	private String idProduct;
	@ApiModelProperty(value = "id da offerta", example = "123")
	private String idOffer;
	@ApiModelProperty(value = "descrição do produto", example="tomada")
	private String description;
	@ApiModelProperty(value = "quantidade do produto", example = "10")
	private String theamount;
}
