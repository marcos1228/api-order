package com.order.domain.dto.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class OrderRequestDto {
	@ApiModelProperty(value = "id do pedido", example = "123")
	private String id;
	@ApiModelProperty(value = "id do produto", example = "123")
	private String idProduct;
	@ApiModelProperty(value = "id da offer", example = "123")
	private String idOffer;
	@ApiModelProperty(value = "descrição do pedido", example = "luva")
	private String description;
	@ApiModelProperty(value = "quantidade", example = "10")
	private String theamount;
}
